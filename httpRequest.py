import requests
from bs4 import BeautifulSoup

url = input("Please input your url and end with Enter.")

page = requests.get(url)
times = 0

while page.status_code != 200 and times < 5:
    page = requests.get(url)
    times += 1

soup = BeautifulSoup(page.text)

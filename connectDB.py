from pymongo import MongoClient
from random import randint

# from pprint import pprint

dbClient = MongoClient("127.0.0.1:27017")

db = dbClient.business

names = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
company_type = ['Inc', 'Company', 'Organization']
company_cuisine = ['Pizza', 'Bar Food', 'Fast Food']

for x in range(0, 20):
    business = {
        'name': names[randint(0, (len(names) - 1))] + ' ' + company_type[randint(0, (len(company_type) - 1))],
        'rating': randint(0, 5),
        'cuisine': company_cuisine[randint(0, (len(company_cuisine) - 1))]
    }

    result = db.reviews.insert_one(business)

print('Created {0} of 20 as {1}'.format(x, result.inserted_id))

print('finished')

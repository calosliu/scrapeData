import requests
from bs4 import BeautifulSoup
import time

USER_INFO = dict(_APP_KEY='1694043833',
                 _APP_SECRET='bf42515208fb43497dd208582a1fa263',
                 _CALLBACK_URL='http://calosliu.space/login')


class WeiboClient(object):
    """docstring for WeiboClient"""

    def __init__(self):
        super(WeiboClient, self).__init__()
        self.client_id = USER_INFO['_APP_KEY']
        self.client_secret = USER_INFO['_APP_SECRET']
        self.redirect_uri = USER_INFO['_CALLBACK_URL']

    def set_code(self):
        print('https://api.weibo.com/oauth2/authorize?client_id={0}&response_type=code&redirect_uri={1}'.format(
            self.client_id, self.redirect_uri))
        self.change_code()

    def request_access_token(self):

        payload = {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'redirect_uri': self.redirect_uri,
            'code': self.code,
            'grant_type': 'authorization_code'
        }
        resp = requests.post(
            'https://api.weibo.com/oauth2/access_token', params=payload).json()

        print(resp)

        return resp

    def change_code(self):
        self.code = input('input your code: ')

    def set_access_token(self, token_resp):
        current = int(time.time())
        self.access_token = str(token_resp['access_token'])
        self.expires = float(token_resp['expires_in'] + current)

    def __getattr__(self, attr):
        return _Callable(self.access_token, attr)


class _Callable(object):
    """docstring for _Callable"""

    def __init__(self, access_token, path):
        super(_Callable, self).__init__()
        self.access_token = access_token
        self.path = path
        print('_Callable: ', access_token, path)

    def __getattr__(self, attr):
        if attr == 'get' or attr == 'post':
            return _Executable(self.access_token, attr.upper(), self.path)
        return _Callable(self.access_token, '{0}/{1}'.format(self.path, attr))


class _Executable(object):
    """docstring for _Executable"""

    def __init__(self, access_token, method, path):
        super(_Executable, self).__init__()
        self.access_token = access_token
        self.method = method
        self.path = path
        print('_Executable: ', access_token, path, method)

    def __call__(self, **kw):
        url = 'https://api.weibo.com/2/{0}.json'.format(self.path)
        return self.weibo_call(url, self.method, self.access_token, **kw)

    def weibo_call(self, url, method, access_token, **kw):
        dict_token = {'access_token': access_token}
        if method == 'GET':
            result = requests.get(url, params={**dict_token, **kw})
        elif method == 'POST':
            result = requests.post(url,)
        else:
            print('unknown operation')
            return

        return BeautifulSoup(result.text, "html.parser")

import sys
import json

from weiboClient import *


def getClient(*token_object):

    client = WeiboClient()

    if len(token_object) == 0:
        client.set_code()
        access_resp = client.request_access_token()
    else:
        access_resp = json.loads(token_object[0])
        print(access_resp)

    client.set_access_token(access_resp)

    return client

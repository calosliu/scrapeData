import sys
import pdb
import logging

# debug
pdb.set_trace()

# logging
logger1 = logging.getLogger("a.b")
logger2 = logging.getLogger("c")

handler = logging.FileHandler("tmp.log", "a")
handler.setFormatter(logging.Formatter("%(name)s %(levelname)s %(asctime)s"))
handler.filter("b")
logger1.addHandler(handler)
logger1.setLevel(logging.INFO)

logger1.debug("debug")
logger1.info("info")
logger1.warning("warning")

print(sys.argv[0])
